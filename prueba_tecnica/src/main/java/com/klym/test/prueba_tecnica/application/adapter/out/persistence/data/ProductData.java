package com.klym.test.prueba_tecnica.application.adapter.out.persistence.data;

import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "PRODUCTS")
public record ProductData(@Id Long id, Double price, String description, String name) {}
