package com.klym.test.prueba_tecnica.application.config;

import com.klym.test.prueba_tecnica.application.ports.in.ProductsUseCase;
import com.klym.test.prueba_tecnica.application.ports.out.ConversionsToLocalMoneyPort;
import com.klym.test.prueba_tecnica.application.ports.out.ProductsPort;
import com.klym.test.prueba_tecnica.application.services.ProductsService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UseConfig {

  @Bean
  public ProductsUseCase productsUseCaseConfig(
      ProductsPort productsPort, ConversionsToLocalMoneyPort conversionsToLocalMoneyPort) {
    return new ProductsService(productsPort, conversionsToLocalMoneyPort);
  }
}
