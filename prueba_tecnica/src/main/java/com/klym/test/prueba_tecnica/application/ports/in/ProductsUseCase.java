package com.klym.test.prueba_tecnica.application.ports.in;

import com.klym.test.prueba_tecnica.domain.Product;

import java.util.List;

public interface ProductsUseCase {

    List<Product> getAllProductsInMyLocal(Money money);

}
