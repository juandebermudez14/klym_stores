package com.klym.test.prueba_tecnica.application.adapter.out.httpclient;

import com.klym.test.prueba_tecnica.application.ports.in.Money;
import com.klym.test.prueba_tecnica.application.ports.out.ConversionsToLocalMoneyPort;
import com.klym.test.prueba_tecnica.domain.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@RequiredArgsConstructor
@Service
public class ConversionToLocalMoneyAdapter implements ConversionsToLocalMoneyPort {

  private final RestTemplate restTemplate;

  private final String END_POINT_API_RATE_CONVERSION = "localhost:1080/USD/to/COP";

  @Override
  public Double getRate(Money money) {
    Map<String, Double> rate =
        restTemplate
            .exchange(
                END_POINT_API_RATE_CONVERSION,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Map<String, Double>>() {})
            .getBody();
    return rate.get("rate");

  }
}
