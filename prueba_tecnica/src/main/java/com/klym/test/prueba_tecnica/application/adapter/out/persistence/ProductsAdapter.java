package com.klym.test.prueba_tecnica.application.adapter.out.persistence;

import com.klym.test.prueba_tecnica.application.adapter.out.persistence.data.ProductData;
import com.klym.test.prueba_tecnica.application.adapter.out.persistence.repository.ProductRepository;
import com.klym.test.prueba_tecnica.application.ports.out.ProductsPort;
import com.klym.test.prueba_tecnica.domain.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class ProductsAdapter implements ProductsPort {

  private final ProductRepository productRepository;

  @Override
  public List<Product> getAllProducts() {
    return productRepository.findAll().stream().map(getProductFromProductData()).toList();
  }

  private static Function<ProductData, Product> getProductFromProductData() {
    return productData ->
        Product.builder()
            .id(productData.id())
            .name(productData.name())
            .price(productData.price())
            .description(productData.description())
            .build();
  }
}
