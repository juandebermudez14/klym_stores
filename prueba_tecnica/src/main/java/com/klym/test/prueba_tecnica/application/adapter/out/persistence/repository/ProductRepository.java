package com.klym.test.prueba_tecnica.application.adapter.out.persistence.repository;

import com.klym.test.prueba_tecnica.application.adapter.out.persistence.data.ProductData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<ProductData,Long> {}
