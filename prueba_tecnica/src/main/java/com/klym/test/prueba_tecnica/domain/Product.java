package com.klym.test.prueba_tecnica.domain;

import lombok.Builder;

@Builder(toBuilder = true)
public record Product(Long id, Double price, String description, String name) {}
