package com.klym.test.prueba_tecnica.application.ports.out;

import com.klym.test.prueba_tecnica.domain.Product;

import java.util.List;

public interface ProductsPort {

    List<Product> getAllProducts();

}
