package com.klym.test.prueba_tecnica.application.adapter.in.web;

import com.klym.test.prueba_tecnica.application.ports.in.Money;
import com.klym.test.prueba_tecnica.application.ports.in.ProductsUseCase;
import com.klym.test.prueba_tecnica.domain.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("Products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductsUseCase productsUseCase;

    @GetMapping
    public List<Product> getAllProducts(@RequestHeader("money")Money money){
        return productsUseCase.getAllProductsInMyLocal(money);
    }
    @PostMapping
    public Product saveProducts(@ResponseBody Product product){
        return productsUseCase.saveProduct(product);
    }

}
