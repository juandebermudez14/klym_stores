package com.klym.test.prueba_tecnica.application.services;

import com.klym.test.prueba_tecnica.application.ports.in.Money;
import com.klym.test.prueba_tecnica.application.ports.in.ProductsUseCase;
import com.klym.test.prueba_tecnica.application.ports.out.ConversionsToLocalMoneyPort;
import com.klym.test.prueba_tecnica.application.ports.out.ProductsPort;
import com.klym.test.prueba_tecnica.domain.Product;
import java.util.List;
import java.util.function.Function;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ProductsService implements ProductsUseCase {

  private final ProductsPort productsPort;
  private final ConversionsToLocalMoneyPort conversionsToLocalMoneyPort;

  @Override
  public List<Product> getAllProductsInMyLocal(Money money) {
    Double rate = conversionsToLocalMoneyPort.getRate(money);
    return productsPort.getAllProducts().stream().map(convertToLocalMoney(rate)).toList();
  }

  private Function<Product, Product> convertToLocalMoney(Double rate) {
    return product -> product.toBuilder().price(product.price() * rate).build();
  }
}
