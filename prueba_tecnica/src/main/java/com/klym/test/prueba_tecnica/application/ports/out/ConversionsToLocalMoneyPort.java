package com.klym.test.prueba_tecnica.application.ports.out;

import com.klym.test.prueba_tecnica.application.ports.in.Money;

public interface ConversionsToLocalMoneyPort {

    Double getRate(Money money);
}
