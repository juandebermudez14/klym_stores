package com.klym.test.prueba_tecnica.application.services;

import com.klym.test.prueba_tecnica.application.ports.in.Money;
import com.klym.test.prueba_tecnica.application.ports.in.ProductsUseCase;
import com.klym.test.prueba_tecnica.application.ports.out.ConversionsToLocalMoneyPort;
import com.klym.test.prueba_tecnica.application.ports.out.ProductsPort;
import com.klym.test.prueba_tecnica.domain.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;


@ExtendWith(MockitoExtension.class)
class ProductsServiceTest {

  @Mock ProductsPort productsPort;

  @Mock ConversionsToLocalMoneyPort conversionsToLocalMoneyPort;

  ProductsUseCase productsUseCase;

  @BeforeEach
  void initUseCase() {
    productsUseCase = new ProductsService(productsPort, conversionsToLocalMoneyPort);
  }

  @Test
  void when_money_is_colombian_pesos() {

    List<Product> build =
        List.of(
            Product.builder().id(1L).price(10.0).name("product_test").description("test").build());
    Mockito.when(productsPort.getAllProducts()).thenReturn(build);
    Mockito.when(conversionsToLocalMoneyPort.getRate(Money.COP)).thenReturn(2.0);
    List<Product> allProductsInMyLocal = productsUseCase.getAllProductsInMyLocal(Money.COP);
    Assertions.assertEquals(allProductsInMyLocal.get(0).price(),20.0);
  }
}
